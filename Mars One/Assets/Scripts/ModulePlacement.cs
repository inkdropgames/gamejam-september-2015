﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class ModulePlacement : MonoBehaviour
{
	public GameObject[] modules;
	private int selectedModule;
	public Transform parent;
	private GameObject ghostModule;
	private bool moduleSelected = false;
	private List<GameObject> markers = new List<GameObject>();
	private GameObject blockingModule;

	public GameObject doorway;

	GameObject door = null;
	private int rayDir = 0;


	private void Start()
	{
		selectedModule = 0;
	}


	private void Update()
	{
		if (ghostModule != null)
		{
			ghostModule.transform.position = Grid.GetGrid(true);

			if (!CheckPosition(Grid.GetGrid(true)))
			{
				ghostModule.transform.GetChild(0).gameObject.GetComponent<Renderer>().material.color = Color.red;

				//if(blockingModule != null)
				//	blockingModule.transform.GetChild(0).GetComponent<Renderer>().material.color = Color.red;
			}
			else
			{
				ghostModule.transform.GetChild(0).gameObject.GetComponent<Renderer>().material.color = Color.white;

				//if (blockingModule != null)
				//	blockingModule.transform.GetChild(0).GetComponent<Renderer>().material.color = Color.white;
			}
		}


		if (Input.GetMouseButtonDown(0) && moduleSelected)
		{
			Debug.Log(CheckPosition(Grid.GetGrid(true)));
			if (CheckPosition(Grid.GetGrid(true)))
			{
				var module = (GameObject) Instantiate(modules[selectedModule], Grid.GetGrid(true), Quaternion.identity);
				module.transform.parent = parent;
				module.layer = 8;
				module.transform.GetChild(1).gameObject.SetActive(true);
				markers.Add(module.transform.GetChild(0).gameObject);
				door.transform.SetParent(module.transform);
				switch (rayDir)
				{
					case 0:
						door.transform.position = new Vector3(module.transform.position.x + 2.75f, module.transform.position.y, module.transform.position.z);
						break;
					case 1:
						door.transform.position = new Vector3(module.transform.position.x - 2.75f, module.transform.position.y, module.transform.position.z);
						break;
					case 2:
						door.transform.position = new Vector3(module.transform.position.x, module.transform.position.y + 2.75f, module.transform.position.z);
						door.transform.eulerAngles = new Vector3(0,0,90);
						break;
					case 3:
						door.transform.position = new Vector3(module.transform.position.x, module.transform.position.y - 2.75f, module.transform.position.z);
						door.transform.eulerAngles = new Vector3(0, 0, 90);
						break;
				}
				moduleSelected = false;
				Destroy(ghostModule);
				ShowMarkers(false);
				AstarPath.active.Scan();
				EventManager.TriggerEvent("UpdatePathfinding");

				switch (selectedModule)
				{
					case 0: //storages
						ModuleManager.instance.storages.Add(module.transform);
						break;
					case 1: //bedrooms
						ModuleManager.instance.bedrooms.Add(module.transform);
						break;
					case 2: //livingrooms
						ModuleManager.instance.livingrooms.Add(module.transform);
						break;
					case 3: //medics
						ModuleManager.instance.livingrooms.Add(module.transform);
						break;
					case 4: //farms
						ModuleManager.instance.farms.Add(module.transform);
						break;
					case 5: //refinery
						ModuleManager.instance.livingrooms.Add(module.transform);
						break;
					case 6: //airlock
						ModuleManager.instance.airlocks.Add(module.transform);
						break;
					case 7: //researchfacilities
						ModuleManager.instance.researchfacilities.Add(module.transform);
						break;

				}
			}
			else
			{
				DisplayManager.Instance().DisplayMessage("Can't build here!");
			}
		}

		if (Input.GetMouseButtonDown(1) && moduleSelected)
		{
			Destroy(ghostModule);
			moduleSelected = false;
			ShowMarkers(false);	
		}
	}

	public void SelectBuilding(int moduleID)
	{
		selectedModule = moduleID;
		ghostModule = (GameObject) Instantiate(modules[selectedModule], Grid.GetGrid(true), Quaternion.identity);
		moduleSelected = true;
		//ShowMarkers(true);
	}

	public bool CheckPosition(Vector2 position)
	{
		RaycastHit2D hit = Physics2D.BoxCast(position, new Vector2(4,4), 2f, Vector2.one*2, Mathf.Infinity);

		if (hit.collider.tag == "Terrain")
		{
			RaycastHit2D[] hit1 = Physics2D.RaycastAll(Grid.GetGrid(true), Vector2.right, 4f);
			RaycastHit2D[] hit2 = Physics2D.RaycastAll(Grid.GetGrid(true), Vector2.left, 4f);
			RaycastHit2D[] hit3 = Physics2D.RaycastAll(Grid.GetGrid(true), Vector2.up, 4f);
			RaycastHit2D[] hit4 = Physics2D.RaycastAll(Grid.GetGrid(true), Vector2.down, 4f);

			RaycastHit2D[] avoidHit1 = Physics2D.RaycastAll(Grid.GetGrid(true), Vector2.right, 3f);
			RaycastHit2D[] avoidHit2 = Physics2D.RaycastAll(Grid.GetGrid(true), Vector2.left, 3f);
			RaycastHit2D[] avoidHit3 = Physics2D.RaycastAll(Grid.GetGrid(true), Vector2.up, 3f);
			RaycastHit2D[] avoidHit4 = Physics2D.RaycastAll(Grid.GetGrid(true), Vector2.down, 3f);

			Debug.DrawRay(Grid.GetGrid(true), Vector2.right * 3.5f);
			Debug.DrawRay(Grid.GetGrid(true), Vector2.left * 3.5f);
			Debug.DrawRay(Grid.GetGrid(true), Vector2.up * 3.5f);
			Debug.DrawRay(Grid.GetGrid(true), Vector2.down * 3.5f);

			if (avoidHit1.Any(objectHit => objectHit.collider.tag != "Terrain"))
				return false;

			if (avoidHit2.Any(objectHit => objectHit.collider.tag != "Terrain"))
				return false;

			if (avoidHit3.Any(objectHit => objectHit.collider.tag != "Terrain"))
				return false;

			if (avoidHit4.Any(objectHit => objectHit.collider.tag != "Terrain"))
				return false;



			
			foreach (RaycastHit2D colHit in hit1)
			{
				if (colHit.collider.tag == "wall")
				{
					door = Instantiate(doorway);
					rayDir = 0;
					return true;
				}
			}


			foreach (RaycastHit2D colHit in hit2)
			{
				if (colHit.collider.tag == "wall")
				{
					door = Instantiate(doorway);
					rayDir = 1;
					return true;
				}
			}

			foreach (RaycastHit2D colHit in hit3)
			{
				if (colHit.collider.tag == "wall")
				{
					door = Instantiate(doorway);
					rayDir = 2;
					return true;
				}
			}

			foreach (RaycastHit2D colHit in hit4)
			{
				if (colHit.collider.tag == "wall")
				{
					door = Instantiate(doorway);
					rayDir = 3;
					return true;
				}
			}

			return false;

		}

		//blockingModule = hit.collider.gameObject;
		return false;
	}

	public void ShowMarkers (bool show)
	{
		foreach (GameObject marker in markers)
		{
			marker.SetActive(show);
		}					
	}
}
