﻿using UnityEngine;
using System.Collections;
public static class Grid
{
	public static Vector2 GetGrid(bool evenSize)
	{
		Vector2 placePos = new Vector2(0, 0);
		//Ray2D ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
		
		placePos = hit.point;
		//placePos.z -= .5f;
		placePos.x = Mathf.Round(placePos.x); if (evenSize) { placePos.x -= 0f; }
		placePos.y = Mathf.Round(placePos.y); if (evenSize) { placePos.y -= 0f; }
		return placePos;	
	}

	public static Vector2 GetGrid()
	{
		Vector2 placePos = new Vector2(0, 0);
		//Ray2D ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

		placePos = hit.point;
		//placePos.z -= .5f;
		placePos.x = Mathf.Round(placePos.x);
		placePos.y = Mathf.Round(placePos.y);

		if (IsOdd((int)placePos.x))
			placePos.x++;

		if (IsOdd((int)placePos.y))
			placePos.y++;
		return placePos;
	}



	//returns -1 when to the left, 1 to the right, and 0 for forward/backward
	public static float AngleDir(Vector3 fwd, Vector3 targetDir, Vector3 up)
	{
		Vector3 perp = Vector3.Cross(fwd, targetDir);
		float dir = Vector3.Dot(perp, up);

		if (dir > 0.0f)
		{
			return 1.0f;
		}
		else if (dir < 0.0f)
		{
			return -1.0f;
		}
		else
		{
			return 0.0f;
		}
	}

	public static bool IsOdd(int value)
	{
		return value % 2 != 0;
	}

}
