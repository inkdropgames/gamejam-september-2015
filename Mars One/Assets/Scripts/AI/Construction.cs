﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class Construction : MonoBehaviour
{

	public GameObject gameManager;
	public GameObject underConstruction;
	public bool working = false;

	public float constructionTime = 5;
	public float timer;

	HallwayPlacement hallwayManager;
	private List<GameObject> hallwayConstruction = new List<GameObject>(); 
	private List<GameObject> hallways;

	private Transform target;

	void Start()
	{
		//gameManager = GameObject.Find("GameManager");
		hallwayManager = gameManager.GetComponent<HallwayPlacement>();
		hallways = gameManager.GetComponent<HallwayPlacement>().hallways;;
		hallwayConstruction = gameManager.GetComponent<HallwayPlacement>().hallwayConstructions;
	}

	void Update()
	{
		if (target != null)
		{
			if (Vector2.Distance(transform.position, target.position) < 1f)
			{
				working = true;
			}

			if (working)
			{
				timer += Time.deltaTime;
				if (timer >= constructionTime)
				{
					if (hallwayConstruction.Count >= 1)
					{
						hallwayConstruction[0].GetComponent<SpriteRenderer>().color = Color.white;
						hallways.Add(hallwayConstruction[0]);
						hallwayConstruction.Remove(hallwayConstruction[0]);
						hallwayManager.UpdateHallways();
						timer = 0;
						BuildNextConstruction();
					}	
				}
				working = false;
			}
		}
	}
	

	public void BuildNextConstruction ()
	{
		if(hallwayConstruction.Count != 0)
		{
			GetComponent<PathFinding>().targetPos = hallwayConstruction[0].transform;
			EventManager.TriggerEvent("UpdatePathfinding");

			target = hallwayConstruction[0].transform;
		}
		else
		{
			GetComponent<AIBehaviour>().working = false;
			GetComponent<AIBehaviour>().moving = false;
			GetComponent<AIBehaviour>().work = false;
		}										  
	}

}
