﻿using Pathfinding;
using UnityEngine;
using System.Collections;

public class PathFinding : MonoBehaviour
{

	public Transform targetPos;
	public Transform sprite;

	private Seeker seeker;
	private Rigidbody2D rb;

	public Path path;

	public float speed = 300f;
	public ForceMode2D fMode;

	[HideInInspector] public bool PathIsEnded = false;

	public float nextWaypointDistance = 3;

	private int currentWayPoint = 0;

	void Start()
	{
		EventManager.StartListening("UpdatePathfinding", UpdatePath);

		seeker = GetComponent<Seeker>();
		rb = GetComponent<Rigidbody2D>();

		if (targetPos == null)
			return;

		seeker.StartPath(transform.position, targetPos.position, OnPathComplete);

		

	}

	public void OnPathComplete(Path p)
	{
		Debug.Log("Possible error" + p.error);
		if (!p.error)
		{
			path = p;
			currentWayPoint = 0;
		}
	}

	public void UpdatePath ()
	{
		if (targetPos == null)
			return;

		seeker.StartPath(transform.position, targetPos.position, OnPathComplete);


	}

	void FixedUpdate()
	{
		if (targetPos == null)
			return;

		if (path == null)
			return;

		if (currentWayPoint >= path.vectorPath.Count)
		{
			if (PathIsEnded)
				return;

			Debug.Log("Distination Reached!");
			GetComponent<AIBehaviour>().moving = false;
			PathIsEnded = true;
			return;
		}
		PathIsEnded = false;

		Vector3 dir = (path.vectorPath[currentWayPoint] - transform.position).normalized;
		dir *= speed*Time.fixedDeltaTime;

		//rb.AddForce(dir, fMode);
		transform.Translate(dir * Time.fixedDeltaTime / 2);

		if (Vector3.Distance(transform.position, path.vectorPath[currentWayPoint]) < nextWaypointDistance)
		{
			currentWayPoint++;
			if (path.vectorPath.Count > currentWayPoint)
			{
				Vector3 direction = path.vectorPath[currentWayPoint] - transform.position;
				float angle = Mathf.Atan2(direction.y, direction.x)*Mathf.Rad2Deg;
				sprite.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
			}
			return;
		}

		


	}

}
