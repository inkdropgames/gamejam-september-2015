﻿using UnityEngine;
using System.Collections;

public class AIBehaviour : MonoBehaviour {

	//Jobs
	public bool construction;
	public bool gathering;
	public bool research;
	public bool medic;

	//Stats
	public int health;
	public int moral;
	public int bladder;
	public int hunger;
	public int thirst;
	public int sleep;
	public bool work;

	//States
	public bool carrying;
	public bool working;
	public bool sleeping;
	public bool eating;
	public bool moving;

	//Inventory
	public int metal;
	public int silicon;
	public int stone;

	//Claimed
	public int bedroom = 0;

	public GameObject sprite;

	void Start()
	{
		InvokeRepeating("UpdateStats", 60, 60f);
		EventManager.StartListening("Work", Work);
	}

	void Update()
	{

		if (!moving && !sleeping)
		{
			if (carrying && ModuleManager.instance.storages.Count >= 1)
			{
				MoveTo("Storage");
				moving = true;
			}
			else if (bladder > 8 && ModuleManager.instance.bathrooms.Count >= 1)
			{
				MoveTo("Bathroom");
				moving = true;
			}
			else if (hunger > 8 && TimeManager.currentHour >= 18 && TimeManager.currentHour <= 19 && ModuleManager.instance.livingrooms.Count >= 1)
			{
				MoveTo("Livingroom");
				moving = true;
			}
			else if (TimeManager.currentHour > 10 && TimeManager.currentHour < 18 && work && !working)
			{
				GetComponent<Construction>().BuildNextConstruction();
				moving = true;
				working = true;
			}
			else if (health <= 2 && ModuleManager.instance.medics.Count >= 1)
			{
				MoveTo("Medic");
				moving = true;
			}
			else if (sleep >= 6 && ModuleManager.instance.bedrooms.Count >= 1)
			{
				Debug.Log("going to sleep");
				MoveTo("Bedroom");
				moving = true;
			}
			
		}

	}

	void MoveTo(string module)
	{
		switch (module)
		{
			case "Storage":
				GetComponent<PathFinding>().targetPos = ModuleManager.instance.storages[Random.Range(0, ModuleManager.instance.storages.Count)];
				EventManager.TriggerEvent("UpdatePathfinding");
				break;
			case "Bathroom":
				GetComponent<PathFinding>().targetPos = ModuleManager.instance.bathrooms[Random.Range(0, ModuleManager.instance.bathrooms.Count)];
				EventManager.TriggerEvent("UpdatePathfinding");
				break;
			case "Livingroom":
				GetComponent<PathFinding>().targetPos = ModuleManager.instance.livingrooms[Random.Range(0, ModuleManager.instance.livingrooms.Count)];
				EventManager.TriggerEvent("UpdatePathfinding");
				break;
			case "Medic":
				GetComponent<PathFinding>().targetPos = ModuleManager.instance.medics[Random.Range(0, ModuleManager.instance.medics.Count)];
				EventManager.TriggerEvent("UpdatePathfinding");
				break;
			case "Bedroom":
				if (Vector2.Distance(ModuleManager.instance.bedrooms[bedroom].position, transform.position) < 5)
				{
					sprite.SetActive(false);
					ModuleManager.instance.bedrooms[bedroom].GetChild(2).gameObject.SetActive(true);
					StartCoroutine("Sleep");
					sleeping = true;
				}
				GetComponent<PathFinding>().targetPos = ModuleManager.instance.bedrooms[bedroom];
				EventManager.TriggerEvent("UpdatePathfinding");
				break;
		}	
	}

	void Work()
	{
		work = true;
	}

	IEnumerator Sleep()
	{
		yield return new WaitForSeconds(60 * sleep);
		sleep = 0;
		sleeping = false;
		sprite.SetActive(true);
		ModuleManager.instance.bedrooms[bedroom].GetChild(2).gameObject.SetActive(false);
	}

	void OnEnable()
	{
		GetComponent<Construction>().BuildNextConstruction();
	}

	void UpdateStats()
	{
		hunger += Random.Range(0, 1);
		thirst += Random.Range(0, 1);
		sleep += Random.Range(0, 1);
		bladder += Random.Range(0, 1);
		health -= Random.Range(0, 1);

		if (hunger > 8 || thirst > 8 || sleep > 9 || bladder > 8)
		{
			moral--;
		}

	}
}
