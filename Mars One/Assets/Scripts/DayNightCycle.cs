﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DayNightCycle : MonoBehaviour
{
	private int dayLength;   //in minutes
	private int dayStart;
	private int nightStart;   // in minutes
	private int currentTime;
	public float cycleSpeed;
    static public bool isDay;
	private Vector3 sunPosition;
	public Light sun;
	public GameObject sunOrbit;

	public Text clock;


	void Start()
	{
		dayLength = 1440;
		dayStart = 300;
		nightStart = 1000;
		currentTime = 720;
		StartCoroutine(TimeOfDay());
		sunOrbit = gameObject.transform.parent.gameObject;
	}
	void Update()
	{

		if (currentTime > 0 && currentTime < dayStart)
		{
			isDay = false;
			GameObject[] lights = GameObject.FindGameObjectsWithTag("light");
			foreach (var light in lights)
			{
				light.GetComponent<Light>().enabled = true;
			}
		}
		else if (currentTime >= dayStart && currentTime < nightStart)
		{
			isDay = true;
			GameObject[] lights = GameObject.FindGameObjectsWithTag("light");
			foreach (var light in lights)
			{
				light.GetComponent<Light>().enabled = false;
			}
		}
		else if (currentTime >= nightStart && currentTime < dayLength)
		{
			isDay = false;
			GameObject[] lights = GameObject.FindGameObjectsWithTag("light");
			foreach (var light in lights)
			{
				light.GetComponent<Light>().enabled = true;
			}
		}
		else if (currentTime >= dayLength)
		{
			currentTime = 0;
		}
		float currentTimeF = currentTime;
		float dayLengthF = dayLength;
		sunOrbit.transform.eulerAngles = new Vector3((-(currentTimeF / dayLengthF) * 360) + 90, 0, 0);
	}

	IEnumerator TimeOfDay()
	{
		while (true)
		{
			currentTime += 1;
			int hours = Mathf.RoundToInt(currentTime / 60);
			int minutes = currentTime % 60;
			clock.text = string.Format("{0:00}:{1:00}", hours, minutes);
			TimeManager.currentHour = hours;
			TimeManager.currentMinute = minutes;
			if (hours + minutes == 0)
				TimeManager.currentDay++;

			yield return new WaitForSeconds(1F / cycleSpeed);
		}
	}
}
