﻿using UnityEngine;
using System.Collections;

public class WaterPumpBehaviour : MonoBehaviour {

    public int waterProduction;
    public float powerUse;
    public bool on = true;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (on)
        {
            if (ResourceManager.totalPower > 0) //if got power
            {
                ResourceManager.totalPower -= powerUse * Time.deltaTime; //use power
                ResourceManager.totalWater += waterProduction * Time.deltaTime; //get water
            }
            else
            {
                DisplayManager.Instance().DisplayMessage("Out of power");
            }
        }
	}
}
