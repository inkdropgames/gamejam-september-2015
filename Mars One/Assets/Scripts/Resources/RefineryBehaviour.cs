﻿using UnityEngine;
using System.Collections;

public class RefineryBehaviour : MonoBehaviour {

    //to change production change "refState" to 1/2/3.
    //to get res, call PickUp(Stone/Metal/Silicon) and fix player inv.
    //to stop, change "refState" to 0

    public int refState = 0; //0=idle, 1=stone, 2=metal, 3=silicon.
    public float powerUse;
    public float stoneProduction;
    private float tempStone;
    public float metalProduction;
    private float tempMetal;
    public float siliconProduction;
    private float tempSilicon;
    public int localStone;
    public int localMetal;
    public int localSilicon;
    
	void Update () {
        if (refState != 0) //if not idle
        {
            if (ResourceManager.totalPower > 0) //And got power
            {
                ResourceManager.totalPower -= powerUse * Time.deltaTime; //use power
                if (refState == 1) //make right resourse
                {
                    tempStone += stoneProduction * Time.deltaTime; //counting towards stone
                    if (tempStone >= 1)//converting to int
                    {
                        tempStone--;
                        localStone++; //adding stone
                    }
                }
                else if (refState == 2)
                {
                    tempMetal += metalProduction * Time.deltaTime;
                    if (tempMetal >= 1)
                    {
                        tempMetal--;
                        localMetal++;
                    }
                }
                else if (refState == 3)
                {
                    tempSilicon += siliconProduction * Time.deltaTime;
                    if (tempSilicon >= 1)
                    {
                        tempSilicon--;
                        localSilicon++;
                    }
                }
            }
            else
            {
                DisplayManager.Instance().DisplayMessage("Out of power");
            }
        }
	}

    void PickUpStone()
    {
        //playerInv += localStone;
    }

    void PickUpMetal()
    {
        //playerInv += localMetal;
    }

    void PickUpSilicon()
    {
        //playerInv += localSilicon;
    }

}
