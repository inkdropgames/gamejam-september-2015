﻿using UnityEngine;
using System.Collections;

public class ResourceManager : MonoBehaviour {

    public static int totalFood;
    public static int totalMetal;
    public static int totalStone;
    public static int totalSilicon;
    public static int totalEarthstuff;
    public static int totalMedicine;
    public static float totalWater;
    public static float totalPower;
    public static float totalOxygen;
    public static int totalPeople;
    public static float storage;
    public static int maxStorage;
    public static float emptyStorage;

    public static float research;
    public float personOxygenUse;
    public static bool oxygen = true;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        storage = totalFood + totalEarthstuff + totalMedicine + totalMetal + totalStone + totalSilicon;
        emptyStorage = maxStorage - storage;
        totalOxygen -= personOxygenUse * totalPeople * Time.deltaTime;
        if (totalOxygen <= 0)
        {
            oxygen = false;
            DisplayManager.Instance().DisplayMessage("Out of oxygen");
        }
	}
}
