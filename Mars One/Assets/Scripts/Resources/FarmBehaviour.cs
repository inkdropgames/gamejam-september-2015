﻿using UnityEngine;
using System.Collections;

public class FarmBehaviour : MonoBehaviour
{
    //To harvest, call the method Harvest() if readyForHarvest==true
    //To plant, change "planted" to true
    public bool planted = false;
    public bool readyForHarvest = false;
    public float powerUse;
    public float waterUse;
    public int foodPrHarvest = 200;
    private bool firstFood = true;
    private float timer;
    public float growTime = 576;
    float stage0;
    float stage1;
    float stage2;
    float stage3;
    private bool isStage0 = false;
    private bool isStage1 = false;
    private bool isStage2 = false;
    public Transform plants;
    private GameObject plantController;
    public Sprite plant1;
    public Sprite plant2;
    public Sprite plant3;

    void Start()
    {
        stage0 = growTime;
        stage1 = (growTime/3)*2;
        stage2 = growTime/3;
        stage3 = 0;
    }

    void Update()
    {
        Debug.Log(timer);
        //checks if someone planted food:
        if (planted)
        {
            if (ResourceManager.totalPower == 0) //And got power
            {
                if (ResourceManager.totalWater == 0) //And got water
                {
                    ResourceManager.totalPower -= powerUse * Time.deltaTime; //use power
                    ResourceManager.totalWater -= waterUse * Time.deltaTime; //use water
                    //If first time in "loop", set time
                    if (firstFood)
                    {
                        timer = growTime;
                        firstFood = false;
                        plantController = (GameObject)Instantiate(plants, transform.position, transform.rotation);
                    }
                    timer -= Time.deltaTime;
                    Debug.Log("growing");
                    if (timer <= stage0)
                    { //changing pictures
                        if (timer <= stage1)
                        { //---||---
                            if (timer <= stage2)
                            { // ---||---
                                if (timer <= stage3)
                                { //harvest ready, reset bools
                                    readyForHarvest = true;
                                    firstFood = true;
                                    planted = false;
                                    SpriteRenderer renderer = plantController.gameObject.GetComponent<SpriteRenderer>();
                                    renderer.sprite = plant3;
                                }
                                else if (!isStage2)
                                {
                                    isStage2 = true;
                                    SpriteRenderer renderer = plantController.gameObject.GetComponent<SpriteRenderer>();
                                    renderer.sprite = plant2;
                                }
                            }
                            else if (!isStage1)
                            {
                                isStage1 = true;
                                SpriteRenderer renderer = plantController.gameObject.GetComponent<SpriteRenderer>();
                                renderer.sprite = plant1;
                            }
                        }
                        else if (!isStage0)
                        {
                            isStage0 = true;
                        }
                    }
                }
                else
                {
                    DisplayManager.Instance().DisplayMessage("Out of water");
                }
            }
            else
            {
                DisplayManager.Instance().DisplayMessage("Out of power");
            }
        }
    }
    void Harvest()
    {
        //add food to inv instead (if we go with local resources)
        if (ResourceManager.emptyStorage < foodPrHarvest)
        {
            ResourceManager.totalFood += (int)(ResourceManager.emptyStorage-.5f);
        }
        else
        {
            ResourceManager.totalFood += foodPrHarvest;
        }
        Destroy(plantController.gameObject);
    }
}