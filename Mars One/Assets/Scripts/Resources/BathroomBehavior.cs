﻿using UnityEngine;
using System.Collections;

public class BathroomBehavior : MonoBehaviour {

    public float waterProduction;
    private float powerUse;
    public bool used;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (used)
        {
            if (ResourceManager.totalPower>=0){
                ResourceManager.totalWater =+ waterProduction*Time.deltaTime;

            }
            else
            {
                DisplayManager.Instance().DisplayMessage("Out of power");
            }
        }
	}
}
