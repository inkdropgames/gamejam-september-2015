﻿using UnityEngine;
using System.Collections;

public class SolarPanelBehaviour : MonoBehaviour {

    public int powerProduction;

    	void Update () {
        if (DayNightCycle.isDay == true)
        {
            ResourceManager.totalPower += powerProduction*Time.deltaTime;
        }
	}
}
