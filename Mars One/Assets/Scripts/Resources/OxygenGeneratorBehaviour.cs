﻿using UnityEngine;
using System.Collections;

public class OxygenGeneratorBehaviour : MonoBehaviour {
    
    public float powerUse;
    public float waterUse;
    public float oxygenProduction;
    public bool on = true;
	
	// Update is called once per frame
    void Update()
    {
        if (on)
        {
            if (ResourceManager.totalPower > 0) //And got power
            {
                if (ResourceManager.totalWater > 0) //And got water
                {
                    ResourceManager.totalPower -= powerUse * Time.deltaTime; //use power
                    ResourceManager.totalWater -= waterUse * Time.deltaTime; //use water
                    ResourceManager.totalOxygen += oxygenProduction * Time.deltaTime; //create Oxygen
                }
                else
                {
                    DisplayManager.Instance().DisplayMessage("Out of water");
                }
            }
            else
            {
                DisplayManager.Instance().DisplayMessage("Out of power");
            }
        }
    }
}
