﻿using UnityEngine;
using System.Collections;

public class TimeManager : MonoBehaviour
{

	public static float currentDay;
	public static float currentHour;
	public static float currentMinute;

	public void SetTimeScale(float value)
	{
		Time.timeScale = value;
	}



}
