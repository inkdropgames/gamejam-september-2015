﻿using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using System.Collections;

public class ModuleManager : MonoBehaviour
{

	public  Transform basicModule;
	public  List<Transform> storages = new List<Transform>();
	public  List<Transform> bedrooms = new List<Transform>();
	public  List<Transform> livingrooms = new List<Transform>();
	public  List<Transform> medics = new List<Transform>();
	public  List<Transform> bathrooms = new List<Transform>();
	public  List<Transform> farms = new List<Transform>();
	public  List<Transform> refinery = new List<Transform>();
	public  List<Transform> airlocks = new List<Transform>();
	public  List<Transform> researchfacilities = new List<Transform>();

	private static ModuleManager moduleManager;

	public static ModuleManager instance
	{
		get
		{
			if (!moduleManager)
			{
				moduleManager = FindObjectOfType(typeof(ModuleManager)) as ModuleManager;

				if (!moduleManager)
				{
					Debug.LogError("There needs to be one active EventManger script on a GameObject in your scene.");
				}
			}

			return moduleManager;
		}
	}

}
