﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HallwayPlacement : MonoBehaviour
{
	private GameObject blockingHallway;

	public Transform colliderParent;
	public Transform hallwayParent;

	private bool selected = true;
	private List<GameObject> buildList = new List<GameObject>();
	public List<GameObject> hallwayConstructions = new List<GameObject>();
	public List<GameObject> hallways = new List<GameObject>();
	[SerializeField]private List<Vector2> hallwayTrans = new List<Vector2>();

	private List<GameObject> colliderList = new List<GameObject>(); 

	private DisplayManager displayManager;

	private Vector2 startGridPosition;
	private Vector2 endGridPosition;

	private Vector2 lastMousePosition;

	private Vector2 currentGridPlacement;
	private Vector2 lastGridPlacement;

	public GameObject cubeTestTemp;
	public GameObject cubeTest;

	public GameObject tempHallwayObj;

	public GameObject hallwayObj;

	public Sprite hallwaySprite;
	public Sprite hallwayTsection;
	public Sprite hallwayCrossSection;
	public Sprite hallwayTurn;
	public Sprite hallwayEnd;

	public GameObject hallwayCol;
	public GameObject hallwayTsectionCol;
	public GameObject hallwayCrossSectionCol;
	public GameObject hallwayTurnCol;
	public GameObject hallwayEndCol;

	void Start()
	{
		displayManager = DisplayManager.Instance();
	}

	void Update()
	{
		if (selected)
		{
			//transform.position = Grid.GetGrid(true);
			if (Input.GetMouseButtonDown(0))
			{
				startGridPosition = Grid.GetGrid();
			}
			if (Input.GetMouseButton(0))
			{
				if (Mathf.Abs(Grid.GetGrid().x - startGridPosition.x) > Mathf.Abs(Grid.GetGrid().y - startGridPosition.y))
				{
					if (lastMousePosition != Grid.GetGrid())
					{
						foreach (var buildObject in buildList)
						{
							Destroy(buildObject);	
						}
						buildList.Clear();
						if (Grid.GetGrid().x - startGridPosition.x > 0)
						{
							for (var i = 0; i < Mathf.Abs((Grid.GetGrid().x - startGridPosition.x)/2); i++)
							{
								var tempHallway = (GameObject)Instantiate(tempHallwayObj, new Vector2(startGridPosition.x + i*2, startGridPosition.y), Quaternion.Euler(0, 0, 90));
								if (!buildList.Contains(tempHallway))
									buildList.Add(tempHallway);
							}
						}
						else
						{
							for (var i = 0; i > (Grid.GetGrid().x - startGridPosition.x)/2; i--)
							{
								var tempHallway =(GameObject)Instantiate(tempHallwayObj, new Vector2(startGridPosition.x + i*2, startGridPosition.y),Quaternion.Euler(0, 0, 90));
								if (!buildList.Contains(tempHallway))
									buildList.Add(tempHallway);
							}	
						}
					}
					lastMousePosition = Grid.GetGrid();
				}
				else
				{
					if (lastMousePosition != Grid.GetGrid())
					{
						foreach (var buildObject in buildList)
						{
							Destroy(buildObject);
						}
						buildList.Clear();
						if (Grid.GetGrid().y - startGridPosition.y > 0)
						{
							for (var i = 0; i < Mathf.Abs((Grid.GetGrid().y - startGridPosition.y)/2); i++)
							{
								var tempHallway =
									(GameObject)
										Instantiate(tempHallwayObj, new Vector2(startGridPosition.x, startGridPosition.y + i*2),
											Quaternion.Euler(0, 0, 0));
								if (!buildList.Contains(tempHallway))
									buildList.Add(tempHallway);
							}
						}
						else
						{
							for (var i = 0; i > (Grid.GetGrid().y - startGridPosition.y)/2; i--)
							{
								var tempHallway =
									(GameObject)
										Instantiate(tempHallwayObj, new Vector2(startGridPosition.x, startGridPosition.y + i * 2),
											Quaternion.Euler(0, 0, 0));
								if (!buildList.Contains(tempHallway))
									buildList.Add(tempHallway);
							}	
						}
					}
					lastMousePosition = Grid.GetGrid();
				}
			}

			if (Input.GetMouseButtonUp(0))
			{
				foreach (var tempHallway in buildList)
				{
					var hallway = (GameObject)Instantiate(hallwayObj, tempHallway.transform.position, tempHallway.transform.rotation);
					Destroy(tempHallway, 0);
					hallwayConstructions.Add(hallway);
					hallwayTrans.Add(hallway.transform.position);
					hallway.transform.parent = hallwayParent;
				}
				buildList.Clear();
				UpdateHallways();
				UpdateHallways();
				EventManager.TriggerEvent("Work");


			}
		}
	}

	public bool CheckPosition(Vector2 position)
	{
		RaycastHit2D hit = Physics2D.BoxCast(position, new Vector2(4, 4), 2f, Vector2.one * 2, Mathf.Infinity);

		if (hit.collider.tag == "Terrain")
		{
			return true;
		}


		blockingHallway = hit.collider.gameObject;
		return false;
	}

	public void UpdateHallways()
	{
		foreach (var collider in colliderList)
		{
			Destroy(collider);	
		}
		colliderList.Clear();

		foreach (GameObject hallway in hallways)
		{
			GameObject col = null;
			int roadPlacement = 0;

			Vector3 position = hallway.transform.position;
			if (hallwayTrans.Contains(new Vector2(position.x + 2, position.y)))
			{
				//road to the right;
				roadPlacement += 1;
			}
			if (hallwayTrans.Contains(new Vector2(position.x - 2, position.y)))
			{
				//road to the left;
				roadPlacement += 3;
			}
			if (hallwayTrans.Contains(new Vector2(position.x, position.y + 2)))
			{
				//road untop;
				roadPlacement += 5;
			}
			if (hallwayTrans.Contains(new Vector2(position.x, position.y -2)))
			{
				//road below
				roadPlacement += 10;
			}

			switch (roadPlacement)
			{
				case 1:
					hallway.GetComponent<SpriteRenderer>().sprite = hallwayEnd;
					hallway.transform.eulerAngles = new Vector3(0, 0, 90);
					col = (GameObject)Instantiate(hallwayEndCol, hallway.transform.position, hallway.transform.rotation);
					break;
				case 3:
					hallway.GetComponent<SpriteRenderer>().sprite = hallwayEnd;
					hallway.transform.eulerAngles = new Vector3(0, 0, -90);
					col = (GameObject)Instantiate(hallwayEndCol, hallway.transform.position, hallway.transform.rotation);
					break;
				case 4:
					hallway.GetComponent<SpriteRenderer>().sprite = hallwaySprite;
					//hallway.transform.eulerAngles = new Vector3(0,0,0);
					col = (GameObject)Instantiate(hallwayCol, hallway.transform.position, hallway.transform.rotation);
					break;
				case 10:
					hallway.GetComponent<SpriteRenderer>().sprite = hallwayEnd;
					col = (GameObject)Instantiate(hallwayEndCol, hallway.transform.position, hallway.transform.rotation);
					break;
				case 15:
					//var newSection = (GameObject) Instantiate(hallwayObj, hallway.transform.position, hallway.transform.rotation); 
					hallway.GetComponent<SpriteRenderer>().sprite = hallwaySprite;
					hallway.transform.eulerAngles = new Vector3(0, 0, 180);
					col = (GameObject)Instantiate(hallwayCol, hallway.transform.position, hallway.transform.rotation);
					break;
				case 5:
					hallway.GetComponent<SpriteRenderer>().sprite = hallwayEnd;
					hallway.transform.eulerAngles = new Vector3(0, 0, 180);
					col = (GameObject)Instantiate(hallwayEndCol, hallway.transform.position, hallway.transform.rotation);
					break;
				case 6:
					hallway.GetComponent<SpriteRenderer>().sprite = hallwayTurn;
					hallway.transform.eulerAngles = new Vector3(0,0,90);
					col = (GameObject)Instantiate(hallwayTurnCol, hallway.transform.position, hallway.transform.rotation);
					break;
				case 8:
					hallway.GetComponent<SpriteRenderer>().sprite = hallwayTurn;
					hallway.transform.eulerAngles = new Vector3(0,0,180);
					col = (GameObject)Instantiate(hallwayTurnCol, hallway.transform.position, hallway.transform.rotation);
					break;
				case 9:
					hallway.GetComponent<SpriteRenderer>().sprite = hallwayTsection;
					hallway.transform.eulerAngles = new Vector3(0, 0, 180);
					col = (GameObject)Instantiate(hallwayTsectionCol, hallway.transform.position, hallway.transform.rotation);
					break;
				case 11:
					hallway.GetComponent<SpriteRenderer>().sprite = hallwayTurn;
					hallway.transform.eulerAngles = new Vector3(0,0,0);
					col = (GameObject)Instantiate(hallwayTurnCol, hallway.transform.position, hallway.transform.rotation);
					break;
				case 13:
					hallway.GetComponent<SpriteRenderer>().sprite = hallwayTurn;
					hallway.transform.eulerAngles = new Vector3(0,0,-90);
					col = (GameObject)Instantiate(hallwayTurnCol, hallway.transform.position, hallway.transform.rotation);
					break;
				case 14:
					hallway.GetComponent<SpriteRenderer>().sprite = hallwayTsection;
					hallway.transform.eulerAngles = new Vector3(0, 0, 0);
					col = (GameObject)Instantiate(hallwayTsectionCol, hallway.transform.position, hallway.transform.rotation);
					break;
				case 19:
					hallway.GetComponent<SpriteRenderer>().sprite = hallwayCrossSection;
					hallway.transform.eulerAngles = new Vector3(0, 0, 0);
					col = (GameObject)Instantiate(hallwayCrossSectionCol, hallway.transform.position, hallway.transform.rotation);
					break;
				case 16:
					hallway.GetComponent<SpriteRenderer>().sprite = hallwayTsection;
					hallway.transform.eulerAngles = new Vector3(0, 0, 90);
					col = (GameObject)Instantiate(hallwayTsectionCol, hallway.transform.position, hallway.transform.rotation);
					break;
				case 18:
					hallway.GetComponent<SpriteRenderer>().sprite = hallwayTsection;
					hallway.transform.eulerAngles = new Vector3(0, 0, -90);
					col = (GameObject)Instantiate(hallwayTsectionCol, hallway.transform.position, hallway.transform.rotation);
					break;

			}
			if (col != null) colliderList.Add(col);
			if (col != null) col.transform.parent = colliderParent;
		}
		AstarPath.active.Scan();
		EventManager.TriggerEvent("UpdatePathfinding");
	}
}
