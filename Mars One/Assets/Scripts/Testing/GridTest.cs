﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class GridTest : MonoBehaviour
{

	public GameObject[] modules;
	private int selectedModule;
	public Transform parent;

	// Use this for initialization
	void Start ()
	{
		selectedModule = 0;
	}
	
	// Update is called once per frame
	void Update ()
	{
		transform.position = Grid.GetGrid(true);

		if (Input.GetMouseButtonDown(0) /*!EventSystem.current.IsPointerOverGameObject(-1)*/)
		{
			GameObject module = (GameObject)Instantiate(modules[selectedModule], transform.position, Quaternion.identity);
			module.transform.parent = parent;
		}
	}
}
